package com.company;

public class Main {
    public static void main(String[] args) {
        float firstNumber = 8;
        float secondNumber = 2;
        float thirdNumber = 4;
        if ((firstNumber >= secondNumber) && (secondNumber > thirdNumber))
            System.out.println("Результат: " + (firstNumber * firstNumber + secondNumber * secondNumber));
        if ((firstNumber >= thirdNumber) && (thirdNumber > secondNumber))
            System.out.println("Результат: " + (firstNumber * firstNumber + thirdNumber * thirdNumber));
        if ((secondNumber >= firstNumber) && (firstNumber > thirdNumber))
            System.out.println("Результат: " + (firstNumber * firstNumber + secondNumber * secondNumber));
        if ((secondNumber >= thirdNumber) && (thirdNumber > firstNumber))
            System.out.println("Результат: " + (thirdNumber * thirdNumber + secondNumber * secondNumber));
        if ((thirdNumber >= secondNumber) && (secondNumber > firstNumber))
            System.out.println("Результат: " + (thirdNumber * thirdNumber + secondNumber * secondNumber));
        if ((thirdNumber >= firstNumber) && (firstNumber > secondNumber))
            System.out.println("Результат: " + (firstNumber * firstNumber + thirdNumber * thirdNumber));
    }
}
